package me.theoatbaron.backupmanager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

import me.theoatbaron.backupmanager.objects.Backup;

public class Database {
	
	private static File dir;
	private static File dirBackups;
	private static File dirSchedule;
	
	
	public static void init(){
		setupDirectories();
	}
	
	private static void setupDirectories(){
		//dir = new File(ClassLoader.getSystemClassLoader().getResource(".").getPath());
		dir = new File("C:\\Users\\TheOatBaron\\Desktop\\BackupManager\\");
		dirBackups = new File(dir.getAbsolutePath()+"\\Backups\\");
		dirSchedule = new File(dir.getAbsolutePath()+"\\Schedule\\");
		if(!dir.exists()){
			dir.mkdir();
		}
		if(!dirBackups.exists()){
			dirBackups.mkdir();
		}
		if(!dirSchedule.exists()){
			dirSchedule.mkdir();
		}
	}
	
	public static void save(Backup backup){
		File localDir = new File(dirBackups.getAbsolutePath()+"\\"+backup.getName()+"\\");
		localDir.mkdir();
		File localToFile = new File(localDir.getAbsolutePath()+"\\to.txt");
		try {
			localToFile.createNewFile();
		} catch (IOException e) {
			Logger.getAnonymousLogger().log(Level.WARNING, "Unable to save Backup '"+backup.toString()+"'");
			e.printStackTrace();
			return;
		}
		File localFromFile = new File(localDir.getAbsolutePath()+"\\from.txt");
		try {
			localFromFile.createNewFile();
		} catch (IOException e) {
			Logger.getAnonymousLogger().log(Level.WARNING, "Unable to save Backup '"+backup.toString()+"'");
			e.printStackTrace();
			return;
		}
		
		PrintWriter writer;
		try {
			writer = new PrintWriter(localFromFile, "UTF-8");
			for(Object o : backup.getFromDLM().toArray()){
				writer.println(o);
			}
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			Logger.getAnonymousLogger().log(Level.WARNING, "Unable to save Backup '"+backup.toString()+"'");
			e.printStackTrace();
			return;
		}
		try {
			writer.close();
			writer = new PrintWriter(localToFile, "UTF-8");
			for(Object o : backup.getToDLM().toArray()){
				writer.println((String) o);
			}
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			Logger.getAnonymousLogger().log(Level.WARNING, "Unable to save Backup '"+backup.toString()+"'");
			e.printStackTrace();
			return;
		}
		writer.close();
	}
	
	private static boolean hasFile(File dir, String fileName){
		return new File(dir.getAbsolutePath()+"\\"+fileName+".txt").exists();
	}
}
