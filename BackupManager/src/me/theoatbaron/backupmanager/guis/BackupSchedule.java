package me.theoatbaron.backupmanager.guis;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

public class BackupSchedule extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BackupSchedule frame = new BackupSchedule();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BackupSchedule() {
		
		setTitle("BackupManager - Schedule");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 266);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBackupSchedule = new JLabel("Backup Schedule");
		lblBackupSchedule.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblBackupSchedule.setBounds(10, 11, 151, 23);
		contentPane.add(lblBackupSchedule);
		
		JList schedule = new JList();
		schedule.setBounds(86, 45, 338, 174);
		contentPane.add(schedule);
		
		JButton btnAdd = new JButton("+");
		btnAdd.setBounds(29, 74, 47, 23);
		contentPane.add(btnAdd);
		
		JButton btnRemove = new JButton("-");
		btnRemove.setBounds(29, 108, 47, 23);
		contentPane.add(btnRemove);
		
		JButton btnModify = new JButton("M");
		btnModify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnModify.setBounds(29, 142, 47, 23);
		contentPane.add(btnModify);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setBounds(349, 13, 75, 23);
		contentPane.add(btnSubmit);
		
		JButton btnBackupNow = new JButton("Backup Now");
		btnBackupNow.setBounds(164, 13, 120, 23);
		contentPane.add(btnBackupNow);
		
		this.setVisible(true);
	}
}
