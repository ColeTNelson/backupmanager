package me.theoatbaron.backupmanager.guis;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class MainMenu extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainMenu frame = new MainMenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainMenu() {
		setTitle("BackupManager");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 193, 257);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBackupManager = new JLabel("Backup Manager");
		lblBackupManager.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblBackupManager.setBounds(30, 11, 123, 17);
		contentPane.add(lblBackupManager);
		
		JButton btnBackupSchedule = new JButton("Backup Schedule");
		btnBackupSchedule.setBounds(18, 93, 142, 23);
		contentPane.add(btnBackupSchedule);
		btnBackupSchedule.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new BackupSchedule();
			}
		});
		
		JButton btnCreateBackup = new JButton("Create Backup");
		btnCreateBackup.setBounds(24, 59, 129, 23);
		contentPane.add(btnCreateBackup);
		btnCreateBackup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new ModifyBackup(null);
			}
		});
		
		JButton btnLogs = new JButton("Logs");
		btnLogs.setBounds(51, 128, 76, 23);
		contentPane.add(btnLogs);
		btnLogs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Logs();
			}
		});
		
		JLabel lblV = new JLabel("v1.0");
		lblV.setBounds(32, 34, 46, 14);
		contentPane.add(lblV);
		
		JLabel lblAuthor = new JLabel("Cole Nelson");
		lblAuthor.setBounds(88, 33, 77, 14);
		contentPane.add(lblAuthor);
		
		JLabel lblLastBackup = new JLabel("Last Backup");
		lblLastBackup.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblLastBackup.setBounds(51, 162, 75, 14);
		contentPane.add(lblLastBackup);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setText("6/15/2015 @ 10:22 AM");
		textField.setBounds(10, 187, 155, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		this.setVisible(true);
	}
}
