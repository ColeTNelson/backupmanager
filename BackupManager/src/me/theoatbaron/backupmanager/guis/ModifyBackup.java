package me.theoatbaron.backupmanager.guis;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import me.theoatbaron.backupmanager.objects.Backup;

public class ModifyBackup extends JFrame {

	private JPanel contentPane;
	private JTextField tfBackupName;
	private final Backup backup;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModifyBackup frame = new ModifyBackup(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ModifyBackup(Backup backup) {
		if(backup==null){
			this.backup = new Backup();
		}else{
			this.backup = backup;
		}
		setTitle("BackupManager - Create/Modify");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 609, 291);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new java.io.File("user.home"));
		chooser.setDialogTitle("BackupManager - Select a file/directory");
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		
		JLabel lblModifyBackup = new JLabel("Modify Backup");
		lblModifyBackup.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblModifyBackup.setBounds(10, 11, 116, 19);
		contentPane.add(lblModifyBackup);
		
		JLabel lblBackup = new JLabel("From...");
		lblBackup.setBounds(10, 41, 52, 14);
		contentPane.add(lblBackup);
		
		JLabel lblTo = new JLabel("To...");
		lblTo.setBounds(305, 42, 46, 14);
		contentPane.add(lblTo);
		
		final JList<String> toList = new JList<String>();
		toList.setBounds(374, 40, 209, 200);
		contentPane.add(toList);
		
		final JList<String> fromList = new JList<String>();
		fromList.setBounds(86, 40, 209, 200);
		contentPane.add(fromList);
		
		final DefaultListModel<String> fromDLM = this.backup.getFromDLM();
		fromList.setModel(fromDLM);
		final DefaultListModel<String> toDLM = this.backup.getToDLM();
		toList.setModel(toDLM);
		
		final JButton btnFromAdd = new JButton("+");
		btnFromAdd.setBounds(21, 95, 47, 23);
		contentPane.add(btnFromAdd);
		btnFromAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (chooser.showOpenDialog(btnFromAdd) == JFileChooser.APPROVE_OPTION) {
					String choice = chooser.getSelectedFile().getAbsolutePath();
					if(fromDLM.contains(choice)){
						JOptionPane.showMessageDialog(null, "Ignoring Selection: Already in backup list!");
					}else{
						fromDLM.addElement(choice);
						JOptionPane.showMessageDialog(null, "Added '"+chooser.getSelectedFile().getAbsolutePath()+"'");
					}
				}
			}
		});
		
		JButton btnFromRemove = new JButton("-");
		btnFromRemove.setBounds(21, 129, 47, 23);
		contentPane.add(btnFromRemove);
		btnFromRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(fromList.getSelectedIndex()==-1){
					return;
				}
				fromDLM.removeElement(fromList.getSelectedValue());
			}
		});
		
		final JButton btnFromModify = new JButton("M");
		btnFromModify.setBounds(21, 163, 47, 23);
		contentPane.add(btnFromModify);
		btnFromModify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(fromList.getSelectedIndex()==-1){
					return;
				}
				if (chooser.showOpenDialog(btnFromModify) == JFileChooser.APPROVE_OPTION) {
					String choice = chooser.getSelectedFile().getAbsolutePath();
					if(fromDLM.contains(choice)){
						JOptionPane.showMessageDialog(null, "Ignoring Selection: Already in backup list!");
					}else{
						fromDLM.removeElement(fromList.getSelectedValue());
						fromDLM.addElement(choice);
						JOptionPane.showMessageDialog(null, "Added '"+chooser.getSelectedFile().getAbsolutePath()+"'");
					}
				}
			}
		});
		
		final JButton btnToAdd = new JButton("+");
		btnToAdd.setBounds(309, 95, 47, 23);
		contentPane.add(btnToAdd);
		btnToAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (chooser.showOpenDialog(btnToAdd) == JFileChooser.APPROVE_OPTION) {
					String choice = chooser.getSelectedFile().getAbsolutePath();
					if(toDLM.contains(choice)){
						JOptionPane.showMessageDialog(null, "Ignoring Selection: Already in saving list!");
					}else{
						toDLM.addElement(choice);
						JOptionPane.showMessageDialog(null, "Added '"+chooser.getSelectedFile().getAbsolutePath()+"'");
					}
				}
			}
		});
		
		JButton btnToRemove = new JButton("-");
		btnToRemove.setBounds(310, 129, 47, 23);
		contentPane.add(btnToRemove);
		btnToRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(toList.getSelectedIndex()==-1){
					return;
				}
				toDLM.removeElement(toList.getSelectedValue());
			}
		});
		
		final JButton btnToModify = new JButton("M");
		btnToModify.setBounds(309, 163, 47, 23);
		contentPane.add(btnToModify);
		btnToModify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(toList.getSelectedIndex()==-1){
					return;
				}
				if (chooser.showOpenDialog(btnToModify) == JFileChooser.APPROVE_OPTION) {
					String choice = chooser.getSelectedFile().getAbsolutePath();
					if(toDLM.contains(choice)){
						JOptionPane.showMessageDialog(null, "Ignoring Selection: Already in backup list!");
					}else{
						toDLM.removeElement(toList.getSelectedValue());
						toDLM.addElement(choice);
						JOptionPane.showMessageDialog(null, "Added '"+chooser.getSelectedFile().getAbsolutePath()+"'");
					}
				}
			}
		});
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setBounds(495, 7, 89, 23);
		contentPane.add(btnSubmit);
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				save();
				close();
			}
		});
		
		JLabel lblBackupName = new JLabel("Backup Name:");
		lblBackupName.setBounds(181, 13, 122, 14);
		contentPane.add(lblBackupName);
		
		tfBackupName = new JTextField();
		tfBackupName.setBounds(272, 11, 130, 20);
		contentPane.add(tfBackupName);
		tfBackupName.setColumns(10);
		tfBackupName.setText(this.backup.getName());
		
		this.setVisible(true);
	}
	
	public void save(){
		this.backup.setName(tfBackupName.getText());
		this.backup.save();
	}
	
	public void close(){
		this.dispose();
	}
}
