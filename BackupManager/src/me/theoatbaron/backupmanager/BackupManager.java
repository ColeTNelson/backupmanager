package me.theoatbaron.backupmanager;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.LookAndFeel;
import javax.swing.UIManager;

import me.theoatbaron.backupmanager.guis.MainMenu;

public class BackupManager {
	public static void main(String[] args){
		Database.init();
		BackupManager.setupLookAndFeel();
		new MainMenu();
	}

	private static void setupLookAndFeel() {
	    try {
	    	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    } catch (Throwable ex) {
	    	Logger.getAnonymousLogger().log(Level.INFO, "Could not set Windows Aero Theme, skipping...");
	    } 
	}
}