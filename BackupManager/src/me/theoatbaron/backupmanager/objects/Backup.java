package me.theoatbaron.backupmanager.objects;

import javax.swing.DefaultListModel;

import me.theoatbaron.backupmanager.Database;

public class Backup {
	
	private DefaultListModel<String> fromDLM = new DefaultListModel<String>();
	private DefaultListModel<String> toDLM = new DefaultListModel<String>();
	private String name = "New Backup";
	
	public Backup(){
		
	}

	public void addToDLM(String s){
		toDLM.addElement(s);
	}
	
	public void addFromDLM(String s){
		fromDLM.addElement(s);
	}
	
	public DefaultListModel<String> getFromDLM() {
		return fromDLM;
	}
	
	public void setFromDLM(DefaultListModel<String> fromDLM) {
		this.fromDLM = fromDLM;
	}

	public DefaultListModel<String> getToDLM() {
		return toDLM;
	}

	public void setToDLM(DefaultListModel<String> toDLM) {
		this.toDLM = toDLM;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void save(){
		Database.save(this);
	}
}
